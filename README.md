Learning Spark

  1. Following [First Steps to
     Scala](http://www.artima.com/scalazine/articles/steps.html)
     
     1. ~~Steps 1-7~~

     2. Steps 8-12

  2. Do [basic Scala exercises at Ampcamp](http://ampcamp.berkeley.edu/5/exercises/introduction-to-the-scala-shell.html)
